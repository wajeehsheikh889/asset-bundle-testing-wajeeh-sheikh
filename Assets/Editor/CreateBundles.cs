﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreateBundles : MonoBehaviour {
	[MenuItem("Assets/Build Asset Bundles")]
	static void BuildAllAssetBundles()
	{
		BuildPipeline.BuildAssetBundles ("Assets/AssetBundles",BuildAssetBundleOptions.None,BuildTarget.StandaloneOSXIntel64);
	}
}
